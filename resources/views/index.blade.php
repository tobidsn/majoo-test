<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ url('/') }}/css/bootstrap.min.css">

        <link rel="stylesheet" href="{{ url('css/main.css') }}">

    </head>
    <body>
        <header>
        <!-- Navbar -->
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark shadow-sm">
                <div class="container d-flex justify-content-between">
                  <div class="collapse navbar-collapse" id="navbarExample01">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item active">
                            <a class="nav-link " aria-current="page" href="#"><h3>Majoo Teknologi Indonesia</h3></a>
                        </li>
                    </ul>
                    </div>
                </div>
            </nav>
        <!-- Navbar -->
        </header>
        <main role="main" class="container mt-100">
            <h1>Produk</h1>
            <div class="row product-card">
                <div class="col-md-3 col-sm-6">
                    <div class="card mb-30 flex-fill">
                        <a class="card-img-tiles" href="#" data-abc="true">
                            <div class="inner">
                                <div class="main-img"><img src="/images/standard_repo.png" alt="Category"></div>
                            </div>
                        </a>
                        <div class="card-body text-center ">
                            <h4 class="card-title">Majoo Pro</h4>
                            <p class="text-muted">Rp 2.700.000</p>
                            <p>
                                Explicabo sit sequi aliquid sunt voluptate voluptas.
                            </p>
                        </div>
                        <div class="card-footer text-center border-top-0">
                            <a class="btn btn-outline-primary btn-sm" href="#" data-abc="true">Beli</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="card mb-30 flex-fill">
                        <a class="card-img-tiles" href="#" data-abc="true">
                            <div class="inner">
                                <div class="main-img"><img src="/images/paket-advance.png" alt="Category"></div>
                            </div>
                        </a>
                        <div class="card-body text-center">
                            <h4 class="card-title">Paket Advance</h4>
                            <p class="text-muted">Rp 2.700.000</p>
                            <p>
                                Explicabo sit sequi aliquid sunt voluptate voluptas. Vero laboriosam eius quia voluptas corrupti quibusdam veritatis consequatur.
                            </p>
                        </div>
                        <div class="card-footer text-center border-top-0">
                            <a class="btn btn-outline-primary btn-sm" href="#" data-abc="true">Beli</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="card mb-30">
                        <a class="card-img-tiles" href="#" data-abc="true">
                            <div class="inner">
                                <div class="main-img"><img src="/images/paket-lifestyle.png" alt="Category"></div>
                            </div>
                        </a>
                        <div class="card-body text-center">
                            <h4 class="card-title">Majoo Lifestyle</h4>
                            <p class="text-muted">Rp 2.700.000</p>
                            <p>
                                Explicabo sit sequi aliquid sunt voluptate voluptas. Vero laboriosam eius quia voluptas corrupti quibusdam veritatis consequatur. Quia consequatur sint voluptatem unde nihil.
                            </p>
                        </div>
                        <div class="card-footer text-center border-top-0">
                            <a class="btn btn-outline-primary btn-sm" href="#" data-abc="true">Beli</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="card mb-30">
                        <a class="card-img-tiles" href="#" data-abc="true">
                            <div class="inner">
                                <div class="main-img"><img src="/images/paket-desktop.png" alt="Category"></div>
                            </div>
                        </a>
                        <div class="card-body text-center">
                            <h4 class="card-title">Majoo Dekstop</h4>
                            <p class="text-muted">Rp 2.700.000</p>
                            <p>
                                Explicabo sit sequi aliquid sunt voluptate voluptas. Vero laboriosam eius quia voluptas corrupti quibusdam veritatis consequatur. Quia consequatur sint voluptatem unde nihil. Quia consequatur sint voluptatem unde nihil.
                            </p>
                        </div>
                        <div class="card-footer text-center border-top-0">
                            <a class="btn btn-outline-primary btn-sm" href="#" data-abc="true">Beli</a>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <footer class="blog-footer">
            <p>2019 &copy; PT Majoo Teknologi Indonesia</p>
        </footer>
        <script src="{{ url('/') }}/js/jquery.min.js"></script>
        <script src="{{ url('/') }}/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
