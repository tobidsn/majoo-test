@extends("_admin.master")
@section("content")
<section class="content-header p-2">
   <div class="container-fluid">
       <div class="row mb-0">
           <div class="col-sm-6">
               <h4>{{ $title }} List</h4>
           </div>
           <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                   <li class="breadcrumb-item"><a href="#">Home</a></li>
                   <li class="breadcrumb-item active">{{ $title }}</li>
               </ol>
           </div>
       </div>
   </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <a href="{{ url('magic/users/create') }}" class="btn btn-success btn-sm">
                            Add New
                        </a> 
                    </div> 
                    <div class="p-2 card-body">
                        <div class="text-center overlay" id="spin">
                            <i class='fa fa-spinner fa-spin loading' style="font-size: 100px;"></i>
                        </div>
                        <div style="overflow-x:auto;" id="datalist">
                        </div>
                    </div>
                    <!-- /.card-body --> 
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>
@endsection
@section('javascript')
<script type="text/javascript">
var url = "{{ url('/magic/userlist') }}?";
var only = '{{ Request::query('search') }}';
var urlDelete = '{{ url('/magic/users') }}/';

// menggunakan function pagination
// dist/js/admin.js line 90
$(document).ready(function() {
    dataList.init();
});
</script>
@endsection