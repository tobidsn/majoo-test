@extends("_admin.master")
@section("content")
<section class="content-header p-2">
   <div class="container-fluid">
       <div class="row mb-0">
           <div class="col-sm-6">
               <h4>File Manager</h4>
           </div>
           <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                   <li class="breadcrumb-item"><a href="#">Home</a></li>
                   <li class="breadcrumb-item active">File Manager</li>
               </ol>
           </div>
       </div>
   </div>
</section> 

<section class="content">
	<div class="container-fluid">
    	<iframe width="100%" height="650" frameborder="0" src="{{ url('/') }}/filemanager/dialog.php?akey={{ env('FILE_KEY') }}&type=0"></iframe>
	</div>
</section> 
@endsection