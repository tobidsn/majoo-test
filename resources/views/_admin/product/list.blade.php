<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>#</th>
            <th>Category</th>
            <th>Name</th>
            <th>Price</th>
            <th>Image</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($product as $items=>$item)
        <tr id="row_{{$item->id}}">
            <td>{{ $items + $product->firstItem() }}</td>
            <td>{{ $item->category->name }}</td>
            <td>{{ $item->name }}</td>
            <td>{{ rupiah_display($item->price) }}</td>
            <td>
                @if($item->image)
                    <img src="{{ imageview($item->image) }}" width="150">
                @endif
            </td>
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Actions
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="{{ url('/magic/product/' . $item->id) }}" title="View product"><i class="fa fa-eye"></i> View</a>

                        <a class="dropdown-item" href="{{ url('/magic/product/' . $item->id . '/edit') }}" title="Edit product"><i class="fa fa-pencil"></i> Edit</a>
                         
                        <a class="dropdown-item" onclick="user_action({{$item->id}}, 'destroy')" href="javascript:void(0)"><i class="fa fa-trash"></i> Delete </a>   
                    </div>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
Showing {{$product->lastItem()}} of {{$product->total()}} entries<br>
{!! $product->appends(['search' => $keyword])->render() !!}