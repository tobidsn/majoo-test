@section('header')
<link rel="stylesheet" href="/dist/plugins/select2/select2.min.css">
<style type="text/css">
    .select2-container {
        width: 100% !important;
        padding: 0;
    }
    .select2-container--default
    .select2-selection--multiple
    .select2-selection__rendered li {
        list-style: none;
    }

    .select2-container--default
    .select2-selection--multiple
    .select2-selection__choice {
        background-color: #007bff;
        border-color: #006fe6;
        padding: 1px 10px;
        color: #fff;
    }
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        margin-top: -6px;
    }

    .progress {
        position:relative;
        width:100%;
        border: 1px solid #7F98B2;
        padding: 1px;
        border-radius: 3px;
    }
    .bar {
        background-color: #B4F5B4;
        width:0%;
        height:25px;
        border-radius: 3px;
    }
    .percent {
        position:absolute;
        display:inline-block;
        left:48%;
        color: #7F98B2;
    }
</style>
@endsection

<div class="form-group">
    <label for="category_id">{{ 'Category Id' }}</label>

    <select name="category_id" class="form-control form-control-sm" id="category_id">
        @foreach($categories as $category)
            <option value="{{ $category->id }}" {{ (isset($product->category_id) && $category->id == $product->category_id) ? 'selected' : '' }}>{{ $category->name }}</option>
        @endforeach
    </select>

    <span class="text-danger">{{ $errors->first('category_id') }}</span>

    <p class="help-block"></p>
</div>

<div class="form-group">
    <label for="name">{{ 'Name' }}</label>
    <input class="form-control" name="name" type="text" id="name" placeholder="{{ucfirst('name')}}" value="{{ isset($product->name) ? $product->name : old('name') }}" required>
    <span class="text-danger">{{ $errors->first('name') }}</span>
    <p class="help-block"></p>
</div>
<div class="form-group">
    <label for="price">{{ 'Price' }}</label>
    <input class="form-control" name="price" type="number" id="price" placeholder="{{ucfirst('price')}}" value="{{ isset($product->price) ? $product->price : old('price') }}" required>
    <span class="text-danger">{{ $errors->first('price') }}</span>
    <p class="help-block"></p>
</div>

<div class="form-group">
    <label for="description">{{ 'Description' }}</label>

    <textarea class="form-control wysiwyg-advanced-br" rows="5" name="description" type="textarea" id="description" placeholder="{{ucfirst('description')}}" required>{{ isset($product->description) ? $product->description : old('description') }}</textarea>

    <span class="text-danger">{{ $errors->first('description') }}</span>
    <p class="help-block"></p>
</div>

<div class="form-group">
    <label for="image">{{ 'Image' }}</label><br>
    @if(isset($product->image))
        <img src="{{ imageview($product->image) }}" id="preview-image" style="display: block; margin: 5px 0 10px 0; width: 100px;">
    @endif
    <input class="btn btn-info" data-toggle="modal" data-target="#uploadModal" type="button" value="attach">

    <input class="form-control" name="image" type="hidden" id="image" placeholder="{{ucfirst('image')}}" value="{{ isset($product->image) ? $product->image : old('image') }}" >

    <span class="text-danger">{{ $errors->first('image') }}</span>
    <p class="help-block"></p>
</div>

<div class="form-group">
    <label for="publish">{{ 'Publish' }}</label>
    <select name="publish" class="form-control form-control-sm" id="publish">
        <option value="1" {{ (isset($product->publish) && $product->publish == '1') ? 'selected' : '' }}>Yes</option>
        <option value="0" {{ (isset($product->publish) && $product->publish == '0') ? 'selected' : '' }}>No</option>
    </select>
    <span class="text-danger">{{ $errors->first('publish') }}</span>
    <p class="help-block"></p>
</div>

<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>

@section('javascript')

<!-- Modal -->
<div id="uploadModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">File upload form</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <!-- Form -->
        <form id="form-upload" method="POST" action="{{ route('imageUpload') }}" enctype="multipart/form-data">
          @csrf
            <div class="form-group">
                <input name="image" type="file" class="form-control"><br/>
                <div class="progress mb-2">
                    <div class="bar"></div >
                    <div class="percent">0%</div >
                </div>
                <input type="submit"  value="Upload" class="btn btn-success">
            </div>
        </form>
        <!-- Preview-->
        <div id='preview'></div>
      </div>
    </div>
  </div>
</div>

<!-- Select2 -->
<script src="/dist/plugins/select2/select2.full.min.js"></script>
<script src="https://malsup.github.io/jquery.form.js"></script>
<script type="text/javascript">

    function validate(formData, jqForm, options) {
        var form = jqForm[0];
        if (!form.image.value) {
            alert('Image not found');
            return false;
        }
    }

    $(function () {
        $('#category_id').select2();

        var bar = $('.bar');
        var percent = $('.percent');
        var status = $('#status');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': document.head.querySelector('meta[name="csrf-token"]').content
            }
        });

        $('#form-upload').ajaxForm({
            beforeSubmit: validate,
            beforeSend: function() {
                status.empty();
                var percentVal = '0%';
                var imageValue = $('input[name=image]').fieldValue();
                bar.width(percentVal)
                percent.html(percentVal);
            },
            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal)
                percent.html(percentVal);
            },
            success: function() {
                var percentVal = '100%';
                bar.width(percentVal)
                percent.html(percentVal);
            },
            complete: function(xhr) {
                status.html(xhr.responseText);
                $('#image').val(xhr.responseJSON.file)
                $('#preview-image').attr('src',  '/' + xhr.responseJSON.file)
                setTimeout(function() {
                    $('#uploadModal').modal('hide');
                }, 2000);
                console.log(xhr.responseJSON)
            }
        });

    });

</script>

@endsection