<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "name" => "Gadget",
                "publish" => 1,
                "created_at" => now(),
            ],
            [
                "name" => "Makanan",
                "publish" => 1,
                "created_at" => now(),
            ],
            [
                "name" => "Minuman",
                "publish" => 1,
                "created_at" => now(),
            ]
        ];

        DB::table('categories')->insert($data);
    }
}
