<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $data = [
            [
                "name" => "Majoo Pro",
                "category_id" => rand(1,3),
                "description" => $faker->text(50),
                "price" => 2750000,
                "publish" => 1,
                "created_at" => now(),
            ],
            [
                "name" => "Majoo Advance",
                "category_id" => rand(1,3),
                "description" => $faker->text(100),
                "price" => 2750000,
                "publish" => 1,
                "created_at" => now(),
            ],
            [
                "name" => "Majoo Lifestyle",
                "category_id" => rand(1,3),
                "description" => $faker->text(150),
                "price" => 2750000,
                "publish" => 1,
                "created_at" => now(),
            ],
            [
                "name" => "Majoo Desktop",
                "category_id" => rand(1,3),
                "description" => $faker->text(200),
                "price" => 2750000,
                "publish" => 1,
                "created_at" => now(),
            ]
        ];

        DB::table('products')->insert($data);
    }
}
