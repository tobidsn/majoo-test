<?php

use Illuminate\Database\Seeder;
use App\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	[
	        	'key' => 'site_name',
	        	'value' => 'Laravel'
	        ], 
	        [
	        	'key' => 'site_email',
	        	'value' => 'info@laravel.com'
	        ], 
	        [
	        	'key' => 'site_image',
	        	'value' => 'iFRfo.jpg'
	        ],
	        [
	        	'key' => 'site_meta_title',
	        	'value' => 'Laravel'
	        ],
	        [
	        	'key' => 'site_meta_description',
	        	'value' => 'Laravel'
	        ],
	        [
	        	'key' => 'analytics_id',
	        	'value' => 'analytics_id'
	        ],
	        [
	        	'key' => 'headercode',
	        	'value' => 'headercode'
	        ],
	        [
	        	'key' => 'footercode',
	        	'value' => 'footercode'
	        ],
        ];

        DB::table('settings')->insert($data);

        Setting::create([
            'key' => 'home_header_title',
            'name' => 'Home Header Title',
            'group' => 'HOME',
            'section' => 'Header',
            'help' => null,
            'placeholder' => null,
            'type' => Setting::TYPE_TEXTINPUT,
            'value' => 'Wujudkanlah Impian Anda Bersama Kami',
            'sort' => 1,
            'sort_group' => Setting::SORT_HOME,
            'status' => Setting::STATUS_ACTIVE,
        ]);

        Setting::create([
            'key' => 'home_header_content',
            'name' => 'Home Header',
            'group' => 'HOME',
            'section' => 'Header',
            'help' => null,
            'placeholder' => null,
            'type' => Setting::TYPE_TEXTAREA,
            'value' => 'Wujudkanlah Impian Anda Bersama Kami',
            'sort' => 2,
            'sort_group' => Setting::SORT_HOME,
            'status' => Setting::STATUS_ACTIVE,
        ]);

        Setting::create([
            'key' => 'home_header_image',
            'name' => 'Home Header Image',
            'group' => 'HOME',
            'section' => 'Header',
            'help' => null,
            'placeholder' => null,
            'type' => Setting::TYPE_IMAGE,
            'value' => 'iFRfo.jpg',
            'sort' => 3,
            'sort_group' => Setting::SORT_HOME,
            'status' => Setting::STATUS_ACTIVE,
        ]); 

        Setting::create([
            'key' => 'home_who_title',
            'name' => 'Title',
            'group' => 'HOME',
            'section' => 'Who We Are',
            'help' => null,
            'placeholder' => null,
            'type' => Setting::TYPE_TEXTINPUT,
            'value' => 'Bintang Mas Realindo adalah',
            'sort' => 1,
            'sort_group' => Setting::SORT_HOME,
            'status' => Setting::STATUS_ACTIVE,
        ]);

        Setting::create([
            'key' => 'home_who_content',
            'name' => 'Content',
            'group' => 'HOME',
            'section' => 'Who We Are',
            'help' => null,
            'placeholder' => null,
            'type' => Setting::TYPE_TEXTINPUT,
            'value' => 'Pengembang Komplek',
            'sort' => 2,
            'sort_group' => Setting::SORT_HOME,
            'status' => Setting::STATUS_ACTIVE,
        ]);

        Setting::create([
            'key' => 'home_who_image',
            'name' => 'Image',
            'group' => 'HOME',
            'section' => 'Who We Are',
            'help' => null,
            'placeholder' => null,
            'type' => Setting::TYPE_IMAGE,
            'value' => 'iFRfo.jpg',
            'sort' => 3,
            'sort_group' => Setting::SORT_HOME,
            'status' => Setting::STATUS_ACTIVE,
        ]);

        Setting::create([
            'key' => 'contact_title',
            'name' => 'Title',
            'group' => 'CONTACT US',
            'section' => 'Info',
            'help' => null,
            'placeholder' => null,
            'type' => Setting::TYPE_TEXTINPUT,
            'value' => 'Contact Us',
            'sort' => 1,
            'sort_group' => Setting::SORT_CONTACT_US,
            'status' => Setting::STATUS_ACTIVE,
        ]);

        Setting::create([
            'key' => 'contact_image',
            'name' => 'Header Image',
            'group' => 'CONTACT US',
            'section' => 'Info',
            'help' => null,
            'placeholder' => null,
            'type' => Setting::TYPE_IMAGE,
            'value' => 'iFRfo.jpg',
            'sort' => 2,
            'sort_group' => Setting::SORT_CONTACT_US,
            'status' => Setting::STATUS_ACTIVE,
        ]);


        Setting::create([
            'key' => 'about_title',
            'name' => 'Title',
            'group' => 'ABOUT US',
            'section' => 'Info',
            'help' => null,
            'placeholder' => null,
            'type' => Setting::TYPE_TEXTINPUT,
            'value' => 'About Us',
            'sort' => 1,
            'sort_group' => Setting::SORT_ABOUT_US,
            'status' => Setting::STATUS_ACTIVE,
        ]);
        Setting::create([
            'key' => 'about_image',
            'name' => 'Header Image',
            'group' => 'ABOUT US',
            'section' => 'Info',
            'help' => null,
            'placeholder' => null,
            'type' => Setting::TYPE_IMAGE,
            'value' => 'iFRfo.jpg',
            'sort' => 2,
            'sort_group' => Setting::SORT_ABOUT_US,
            'status' => Setting::STATUS_ACTIVE,
        ]);

        Setting::create([
            'key' => 'about_content_title',
            'name' => 'Content Title',
            'group' => 'ABOUT US',
            'section' => 'Info',
            'help' => null,
            'placeholder' => null,
            'type' => Setting::TYPE_TEXTINPUT,
            'value' => 'Company Profile',
            'sort' => 3,
            'sort_group' => Setting::SORT_ABOUT_US,
            'status' => Setting::STATUS_ACTIVE,
        ]);

        Setting::create([
            'key' => 'about_content',
            'name' => 'Content',
            'group' => 'ABOUT US',
            'section' => 'Info',
            'help' => '',
            'placeholder' => null,
            'type' => Setting::TYPE_TEXTAREA,
            'value' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
            'sort' => 4,
            'sort_group' => Setting::SORT_ABOUT_US,
            'status' => Setting::STATUS_ACTIVE,
        ]);

        Setting::create([
            'key' => 'about_image_1',
            'name' => 'Content Image Small',
            'group' => 'ABOUT US',
            'section' => 'Info',
            'help' => '',
            'placeholder' => null,
            'type' => Setting::TYPE_IMAGE,
            'value' => 'iFRfo.jpg',
            'sort' => 5,
            'sort_group' => Setting::SORT_ABOUT_US,
            'status' => Setting::STATUS_ACTIVE,
        ]);

        Setting::create([
            'key' => 'about_image_2',
            'name' => 'Content Image Big',
            'group' => 'ABOUT US',
            'section' => 'Info',
            'help' => '',
            'placeholder' => null,
            'type' => Setting::TYPE_IMAGE,
            'value' => 'iFRfo.jpg',
            'sort' => 5,
            'sort_group' => Setting::SORT_ABOUT_US,
            'status' => Setting::STATUS_ACTIVE,
        ]);

        Setting::create([
            'key' => 'about_vm_title',
            'name' => 'Title Vision Mission',
            'group' => 'ABOUT US',
            'section' => 'Info',
            'help' => '',
            'placeholder' => null,
            'type' => Setting::TYPE_TEXTINPUT,
            'value' => 'Our Vision & Mission',
            'sort' => 6,
            'sort_group' => Setting::SORT_ABOUT_US,
            'status' => Setting::STATUS_ACTIVE,
        ]);

        Setting::create([
            'key' => 'about_vision',
            'name' => 'Vision Content',
            'group' => 'ABOUT US',
            'section' => 'Info',
            'help' => '',
            'placeholder' => null,
            'type' => Setting::TYPE_TEXTAREA,
            'value' => 'Visi kami adalah',
            'sort' => 7,
            'sort_group' => Setting::SORT_ABOUT_US,
            'status' => Setting::STATUS_ACTIVE,
        ]);


        Setting::create([
            'key' => 'about_mission',
            'name' => 'Mission Content',
            'group' => 'ABOUT US',
            'section' => 'Info',
            'help' => '',
            'placeholder' => null,
            'type' => Setting::TYPE_TEXTAREA,
            'value' => 'Misi kami adalah',
            'sort' => 8,
            'sort_group' => Setting::SORT_ABOUT_US,
            'status' => Setting::STATUS_ACTIVE,
        ]);

        /************** CONTACT INFORMATION **************/
        Setting::create([
            'key' => 'contact_copyright',
            'name' => 'Copyright',
            'group' => 'ALL PAGES',
            'section' => 'Footer',
            'help' => null,
            'placeholder' => null,
            'type' => Setting::TYPE_TEXTAREA,
            'value' => '© 2019 Laravel Inc. All Right Reserved',
            'sort' => 10,
            'sort_group' => Setting::SORT_ALL_PAGES,
            'status' => Setting::STATUS_ACTIVE,
        ]);

        Setting::create([
            'key' => 'contact_we_work',
            'name' => 'We Work',
            'group' => 'ALL PAGES',
            'section' => 'Contact Information',
            'help' => null,
            'placeholder' => null,
            'type' => Setting::TYPE_TEXTAREA,
            'value' => 'Senin - Jumat pukul 09.00 - 17.00
Sabtu pukul 09.00 - 14.00',
            'sort' => 1,
            'sort_group' => Setting::SORT_ALL_PAGES,
            'status' => Setting::STATUS_ACTIVE,
        ]);
        Setting::create([
            'key' => 'contact_office',
            'name' => 'Phone Office',
            'group' => 'ALL PAGES',
            'section' => 'Contact Information',
            'help' => null,
            'placeholder' => null,
            'type' => Setting::TYPE_TEXTINPUT,
            'value' => '(021) 5937 11111',
            'sort' => 2,
            'sort_group' => Setting::SORT_ALL_PAGES,
            'status' => Setting::STATUS_ACTIVE,
        ]);
        Setting::create([
            'key' => 'contact_whatsapp',
            'name' => 'Whatsapp',
            'group' => 'ALL PAGES',
            'section' => 'Contact Information',
            'help' => null,
            'placeholder' => null,
            'type' => Setting::TYPE_TEXTINPUT,
            'value' => '085103009886',
            'sort' => 3,
            'sort_group' => Setting::SORT_ALL_PAGES,
            'status' => Setting::STATUS_ACTIVE,
        ]);
        Setting::create([
            'key' => 'contact_fax',
            'name' => 'FAX Office',
            'group' => 'ALL PAGES',
            'section' => 'Contact Information',
            'help' => null,
            'placeholder' => null,
            'type' => Setting::TYPE_TEXTINPUT,
            'value' => '(021) 558 2777',
            'sort' => 2,
            'sort_group' => Setting::SORT_ALL_PAGES,
            'status' => Setting::STATUS_ACTIVE,
        ]);
        Setting::create([
            'key' => 'contact_ig',
            'name' => 'Instagram',
            'group' => 'ALL PAGES',
            'section' => 'Contact Information',
            'help' => null,
            'placeholder' => null,
            'type' => Setting::TYPE_TEXTINPUT,
            'value' => 'laravelphp',
            'sort' => 2,
            'sort_group' => Setting::SORT_ALL_PAGES,
            'status' => Setting::STATUS_ACTIVE,
        ]);
        Setting::create([
            'key' => 'contact_maps_url',
            'name' => 'Google Maps Url',
            'group' => 'ALL PAGES',
            'section' => 'Contact Information',
            'help' => null,
            'placeholder' => null,
            'type' => Setting::TYPE_TEXTINPUT,
            'value' => 'https://goo.gl/maps/4YYvgZa9M5n',
            'sort' => 4,
            'sort_group' => Setting::SORT_ALL_PAGES,
            'status' => Setting::STATUS_ACTIVE,
        ]);

        Setting::create([
            'key' => 'contact_email_info',
            'name' => 'Email Info',
            'group' => 'ALL PAGES',
            'section' => 'Contact Information',
            'help' => null,
            'placeholder' => null,
            'type' => Setting::TYPE_TEXTINPUT,
            'value' => 'info@laravel.com',
            'sort' => 5,
            'sort_group' => Setting::SORT_ALL_PAGES,
            'status' => Setting::STATUS_ACTIVE,
        ]);

        Setting::create([
            'key' => 'contact_email_sales',
            'name' => 'Email Sales',
            'group' => 'ALL PAGES',
            'section' => 'Contact Information',
            'help' => null,
            'placeholder' => null,
            'type' => Setting::TYPE_TEXTINPUT,
            'value' => 'sales@laravel.com',
            'sort' => 6,
            'sort_group' => Setting::SORT_ALL_PAGES,
            'status' => Setting::STATUS_ACTIVE,
        ]);
        Setting::create([
            'key' => 'contact_address',
            'name' => 'Address',
            'group' => 'ALL PAGES',
            'section' => 'Contact Information',
            'help' => null,
            'placeholder' => null,
            'type' => Setting::TYPE_TEXTAREA,
            'value' => 'Ruko Royal Living Blok RB No. 5-6
Jalan Gatot Subroto,Kedaung,Sepatan,
Tangerang,15520',
            'sort' => 7,
            'sort_group' => Setting::SORT_ALL_PAGES,
            'status' => Setting::STATUS_ACTIVE,
        ]);

    }
}
