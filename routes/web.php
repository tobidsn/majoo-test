<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/product', 'IndexController@product');

Route::get('magic/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('magic/login', 'Auth\LoginController@login')->name('login');
Route::post('magic/logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => 'has_any_role:admin','namespace' => 'Admin'], function() {
    
    Route::prefix('magic')->group(function () {
        Route::resource('users', 'UsersController');
        Route::get('userlist', 'UsersController@list');
    });
    
    Route::get('site/settings', 'SettingController@index')->name('site.settings');
    Route::post('site/settings', 'SettingController@update');

    Route::get('site/website', ['uses' => 'SettingController@all']);
    Route::post('site/save', ['uses' => 'SettingController@save']);
    Route::get('site/flush-cache', ['uses' => 'SettingController@flushCache']);
});

Route::group(['middleware' => 'has_any_role:admin,editor', 'prefix' => 'magic', 'namespace' => 'Admin'], function() {
    Route::get('home', 'DashboardController@versionone');
    Route::get('profile', 'UsersController@profile');
    Route::patch('profile/{id}', 'UsersController@updateProfile'); 

    Route::resource('category', 'CategoryController');
    Route::get('categorylist', 'CategoryController@list');

    Route::resource('product', 'ProductController');
    Route::get('productlist', 'ProductController@list');
    Route::post('image-upload', 'ProductController@imageUpload')->name('imageUpload');
});