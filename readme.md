# Majoo Test

# Installation
```
$ git clone git@bitbucket.org:tobidsn/majoo-test.git
$ cd majoo-test

$ Create a mysql database
$ cp .env.example to .env
$ composer install

$ php artisan key:generate
$ php artisan migrate --seed
$ php artisan serve

$ sudo chgrp -R www-data storage bootstrap/cache public/files
$ sudo chmod -R ug+rwx storage bootstrap/cache public/files
```

## Link
* Admin login : http://127.0.0.1:8000/magic/login

    Email : admin@admin.com
    Password : admin1

* Homepage template: http://127.0.0.1:8000
* Homepage integrated database : http://127.0.0.1:8000/product

## Screenshoot

* Homepage
![Alt text](/Homepage-screenshot.png "homepage")

* Admin - Login
![Alt text](/Login-screenshot.png "")

* Admin - Products
![Alt text](/Products-screenshot.png "")

* Admin - Form Products
![Alt text](/Form-Product-screenshot.png "")

* Admin - Form Products Select2
![Alt text](/Form-Product-Select2-screenshot.png "")

* Admin - Form Products Upload Image
![Alt text](/Form-Product-Upload-Image-screenshot.png "")


## Built With

* [Laravel](https://laravel.com/) - The PHP Framework For Web Artisans
* [Admin LTE 3](https://github.com/almasaeed2010/AdminLTE) - AdminLTE - Free Premium Admin control Panel Theme

## ICONs 

* Font Awesesome

## Maintainer 
* [Tobi](https://twitter.com/tobidsn/)