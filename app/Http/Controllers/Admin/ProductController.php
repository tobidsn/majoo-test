<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Product;
use App\Category;

class ProductController extends Controller
{   
    private $title;

    function __construct()
    {
        $this->title = 'Products';
    }

    public function index(Request $request)
    {   
        return view('_admin.product.index')->with('title', $this->title);
    }

    public function list(Request $request)
    {   
        $keyword = $request->get('only');
        if (!empty($keyword)) {
            $product = Product::where('name', 'LIKE', '%'.$keyword.'%')->latest()->paginate(10);
        } else {
            $product = Product::latest()->paginate(10);
        }
        
        return view('_admin.product.list', compact('product','keyword'));
    }

    public function create()
    {
        $categories = Category::where('publish', 1)->get();
        return view('_admin.product.create', compact('categories'))->with('title', $this->title);
    }

    public function store(ProductRequest $request)
    {   
        $validated = $request->validated();
        
        $data = Product::newRecord($request);

        return redirect('magic/product')->with('success', 'Product added!');
    }

    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $categories = Category::where('publish', 1)->get();
        return view('_admin.product.edit', compact('product','categories'))->with('title', $this->title);
    } 

    public function show($id)
    {
        $product = Product::findOrFail($id);
                
        return view('_admin.product.show', compact('product'))->with('title', $this->title);
    }

    public function update(ProductRequest $request, $id)
    {
        $validated = $request->validated();
        
        $data = Product::updateRecord($request, $id);
        
        return redirect('magic/product')->with('success', 'Product updated!');
    }

    public function destroy(Request $request, $id)
    {
        Product::destroy($id);

        if($request->ajax()){
            return array("message" => 'Product deleted!', "id" => $id);
        } else {
            return redirect('magic/product')->with('success', 'Product deleted!');
        }
    }

    public function imageUpload(Request $request)
    {
        $request->validate([
            'image' => 'required',
        ]);

        $fileName = time().'.'.request()->image->getClientOriginalExtension();
        $folder = 'files/product';
        $file = request()->image->move(public_path($folder), $fileName);

        return response()->json([
            'file' => $folder.'/'.$fileName,
            'success'=>'You have successfully upload file.'
        ]);
    }
}