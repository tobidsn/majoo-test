<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Setting;
use Cache;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return view('_admin.pages.setting');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = array(
            'site_name' => $request->get('site_name'), 
            'site_email' => $request->get('site_email'),
            'site_meta_title' => $request->get('site_meta_title'),
            'site_meta_description' => $request->get('site_meta_description'),
            'analytics_id' => $request->get('analytics_id'),
            'headercode' => $request->get('headercode'),
            'footercode' => $request->get('footercode'),
        );

        foreach ($data as $key => $value) {
            
            Setting::where('key', $key)->update([
                'value' => $value,
            ]);
        }

        return redirect()->route('site.settings')->with('success', 'Setting updated!');
    }

    public function all(Request $request)
    {
        $data = [];
        $settings = Setting::where('status', Setting::STATUS_ACTIVE)
            ->orderBy('sort_group', 'asc')
            ->orderBy('section', 'asc')
            ->orderBy('sort', 'asc')
            ->get();

        foreach ($settings as $i => $setting) {
            $data[$setting->group][$setting->section][] = $setting;
        }

        return view('_admin.settings.all', compact('data'))->with('title', 'All Setting');
    }

    public function save(Request $request)
    {
        foreach ($request->all() as $key => $value) {
            $model = Setting::where('key', $key)->first();

            if (!$model) {
                continue;
            }

            $model->value = is_array($value) ? serialize($value) : $value;
            $model->save();
        }

        Cache::flush();   
        return redirect('/site/website')->with('success', 'Settings updated!');
    }

    public function flushCache(Request $request)
    {
        Cache::flush(); 
        return redirect('/site/website')->with('success', 'Cache cleared!');
    }

    public function filemanager()
    {   
        return view('_admin.pages.filemanager');
    }
}
