<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class IndexController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function product()
    {
        $products = Product::where('publish', 1)->latest()->get();

        return view('product', [
            'products' => $products
        ]);
    }
}
