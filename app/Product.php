<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	 /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    public function category()
    {
        return $this->belongsTo("App\Category", 'category_id');
    }


    public static function newRecord($request)
    {
        $data= new Product;
        $data->category_id = $request->get('category_id');
        $data->name = $request->get('name');
        $data->price = $request->get('price');
        $data->description = $request->get('description');
        $data->image = $request->get('image');
        $data->publish = $request->get('publish');

        $data->save();

        return $data;
    }

    public static function updateRecord($request, $id)
    {
        $data = Product::findOrFail($id);
        $data->category_id = $request->get('category_id');
        $data->name = $request->get('name');
        $data->price = $request->get('price');
        $data->description = $request->get('description');
        $data->image = $request->get('image');
        $data->publish = $request->get('publish');

        $data->save();

        return $data;
    }
}